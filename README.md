# GitlabNotionBot

This is a very BETA project

## Getting started

1. create an env file with following variables
```
NOTION_SECRET=<notion integration secret>
GITLAB_TOKEN=<personal gitlab token secret>
GITLAB_GROUP_ID==<gitlab group id, currently only supporting groups>
DATABASE_ID==<database id>
```

2. Run `node index.js` to add new milestones to notion board.

Milestone name is used as it's unique identifier, changing the milestone name might result in duplicate milestone in notion databse