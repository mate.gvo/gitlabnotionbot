const { Client } = require("@notionhq/client");
const axios = require("axios");
const { parsed: config } = require("dotenv").config();

const notion = new Client({
  auth: config.NOTION_SECRET,
});

const databaseId = config.DATABASE_ID;
const groupId = config.GITLAB_GROUP_ID;

var gitlabRequest = (method) => ({
  method: "get",
  url: `https://gitlab.com/api/v4/groups/${groupId}/${method}`,
  headers: {
    "PRIVATE-TOKEN": config.GITLAB_TOKEN,
  },
});

const makeGitLabRequest = async (method) => {
  return axios(gitlabRequest(method))
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error.code, error.message);
    });
};

async function addItem() {
  const milestones = await makeGitLabRequest("milestones");

  const { results: pages } = await notion.databases.query({
    database_id: databaseId,
  });

  const existingIds = pages.map(
    (page) => page.properties.Name.title[0].plain_text
  );

  try {
    await Promise.all(
      milestones.map((m) => {
        return new Promise((resolve) => {
          if (existingIds.includes(m.title)) return resolve();

          console.log("Adding milestone" + m.title);

          let status = {};
          if (m.state === "closed") {
            status = {
              Status: {
                id: "Q%5B%40x",
                type: "select",
                select: {
                  id: "4c7b4d30-57c2-4f87-b074-39a1cb444302",
                  name: "done",
                  color: "blue",
                },
              },
            };
          }

          notion.pages
            .create({
              parent: { database_id: databaseId },
              properties: {
                ...status,
                title: {
                  title: [
                    {
                      text: {
                        content: m.title,
                      },
                    },
                  ],
                },
              },
            })
            .then(() => resolve());
        });
      })
    );
  } catch (error) {
    console.error(error.body);
  }
}

addItem();
