module.exports = {
    env: {
        node: true,
        browser: true,
        commonjs: true,
        es2021: true,
    },
    plugins: ["prettier"],
    rules: {
        "prettier/prettier": "error",
    },
    extends: ["eslint:recommended", "prettier", "plugin:prettier/recommended"],
    parserOptions: {
        ecmaVersion: 13,
    },
};
